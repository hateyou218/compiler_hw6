#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
#include "symbolTable.h"
#include "offsetInAR.h"
#include "myRegister.h"
#include "printSourceFile.h"

FILE* g_codeGenOutputFp = NULL;
char* g_currentFunctionName = NULL;

int getLabelNumber();
int codeGenConstantLabel(C_type constantType, void* valuePtr);
void codeGenGetBoolOfFloat(int boolRegIndex, int floatRegIndex);
void codeGenPrepareRegister(ProcessorType processorType, int regIndex, int needToBeLoaded, int workRegIndexIfPseudo, char** regName);
void codeGenSaveToMemoryIfPsuedoRegister(ProcessorType processorType, int regIndex, char* regName);
void codeGenFloatCompInstruction(char *instruction, int dstRegIndex, int srcReg1Index, int srcReg2Index);
void codeGenLogicalInstruction(ProcessorType processorType, char *instruction, int dstRegIndex, int srcReg1Index, int srcReg2Index);
//reg1 is dst
void codeGen2RegInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index);
//reg1 is dst
void codeGen3RegInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index, int reg3Index);
void codeGen2Reg1ImmInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index, void* imm);
int codeGenConvertFromIntToFloat(int intRegIndex);
int codeGenConvertFromFloatToInt(int floatRegIndex);
void getExprOrConstValue_(AST_NODE* exprOrConstNode, int* iValue, float* fValue);
void codeGenLocalVariableInitialization(AST_NODE *varaibleDeclListNode);
void setOffsetsOfParameters(AST_NODE * parameterNode); 
//*************************

void codeGenProgramNode(AST_NODE *programNode);
void codeGenGlobalVariable(AST_NODE *varaibleDeclListNode);
void codeGenFunctionDeclaration(AST_NODE *functionDeclNode);
void codeGenGeneralNode(AST_NODE* node);
void codeGenStmtNode(AST_NODE* stmtNode);
void codeGenBlockNode(AST_NODE* blockNode);
void codeGenWhileStmt(AST_NODE* whileStmtNode);
void codeGenForStmt(AST_NODE* forStmtNode);
void codeGenIfStmt(AST_NODE* ifStmtNode);
void codeGenReturnStmt(AST_NODE* returnStmtNode);
void codeGenAssignOrExpr(AST_NODE* testNode);
void codeGenAssignmentStmt(AST_NODE* assignmentStmtNode);
void codeGenExprRelatedNode(AST_NODE* exprRelatedNode);
void codeGenExprNode(AST_NODE* exprNode);
void codeGenFunctionCall(AST_NODE* functionCallNode);
void codeGenVariableReference(AST_NODE* idNode);
void codeGenConstantReference(AST_NODE* constantNode);
int codeGenCalcArrayElemenetAddress(AST_NODE* idNode);

int getLabelNumber()
{
    static int labelNumber = 0;
    return labelNumber++;
}


int codeGenConstantLabel(C_type constantType, void* valuePtr)
{
    int labelNumber = getLabelNumber();
    
    fprintf(g_codeGenOutputFp, ".data\n");

    if(constantType == INTEGERC)
    {
        int* val = (int*)valuePtr;
        fprintf(g_codeGenOutputFp, "_CONSTANT_%d: .word %d\n", labelNumber, *val);
    }
    else if(constantType == FLOATC)
    {
        float* val = (float*)valuePtr;
        fprintf(g_codeGenOutputFp, "_CONSTANT_%d: .float %f\n", labelNumber, *val);
    }
    else if(constantType == STRINGC)
    {
        char* val = (char*)valuePtr;
        fprintf(g_codeGenOutputFp, "_CONSTANT_%d: .asciiz %s\n", labelNumber, val);
        fprintf(g_codeGenOutputFp, ".align 2\n");
    }

    fprintf(g_codeGenOutputFp, ".text\n");

    return labelNumber;
}


void codeGenGetBoolOfFloat(int boolRegIndex, int floatRegIndex)
{
    float zero = 0.0f;
    int constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);

    char* tmpZeroRegName = floatWorkRegisterName[0];
    fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", tmpZeroRegName, constantZeroLabelNumber);
    char* origFloatRegName = NULL;
    codeGenPrepareRegister(FLOAT_REG, floatRegIndex, 1, 1, &origFloatRegName);
    fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", tmpZeroRegName, origFloatRegName);
    
    char* boolRegName = NULL;
    codeGenPrepareRegister(INT_REG, boolRegIndex, 0, 0, &boolRegName);
    int tmpLabelIndex = getLabelNumber();
    fprintf(g_codeGenOutputFp, "bc1t _setFalse_%d\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "li %s, %d\n", boolRegName, 1);
    fprintf(g_codeGenOutputFp, "j _setBoolEnd_%d\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "_setFalse_%d:\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "li %s, %d\n", boolRegName, 0);
    fprintf(g_codeGenOutputFp, "_setBoolEnd_%d:\n", tmpLabelIndex);

    codeGenSaveToMemoryIfPsuedoRegister(INT_REG, boolRegIndex, boolRegName);
}


void codeGenPrepareRegister(ProcessorType processorType, int regIndex, int needToBeLoaded, int workRegIndexIfPseudo, char** regName)
{
    int realRegisterCount = (processorType == INT_REG) ? INT_REGISTER_COUNT : FLOAT_REGISTER_COUNT;
    char** realRegisterName = (processorType == INT_REG) ? intRegisterName : floatRegisterName;
    char** workRegisterName = (processorType == INT_REG) ? intWorkRegisterName : floatWorkRegisterName;
    char* loadInstruction = (processorType == INT_REG) ? "lw" : "l.s";

    if(regIndex >= realRegisterCount)
    {
        //pseudo register
        int pseudoIndex = regIndex - realRegisterCount;
        *regName = workRegisterName[workRegIndexIfPseudo];
        if(needToBeLoaded)
        {
            fprintf(g_codeGenOutputFp, "%s %s, %d($fp)\n", loadInstruction, *regName, getPseudoRegisterCorrespondingOffset(pseudoIndex));
        }
    }
    else
    {
        *regName = realRegisterName[regIndex];
    }
}


void codeGenSaveToMemoryIfPsuedoRegister(ProcessorType processorType, int regIndex, char* regName)
{
    int realRegisterCount = (processorType == INT_REG) ? INT_REGISTER_COUNT : FLOAT_REGISTER_COUNT;
    char* saveInstruction = (processorType == INT_REG) ? "sw" : "s.s";

    if(regIndex >= realRegisterCount)
    {
        //pseudo register
        int pseudoIndex = regIndex - realRegisterCount;
        fprintf(g_codeGenOutputFp, "%s %s, %d($fp)\n", saveInstruction, regName, getPseudoRegisterCorrespondingOffset(pseudoIndex));
    }
}


void codeGenFloatCompInstruction(char *notRealInstruction, int dstRegIndex, int srcReg1Index, int srcReg2Index)
{
    char* srcReg1Name = NULL;
    codeGenPrepareRegister(FLOAT_REG, srcReg1Index, 1, 0, &srcReg1Name);

    char* srcReg2Name = NULL;
    codeGenPrepareRegister(FLOAT_REG, srcReg2Index, 1, 1, &srcReg2Name);

    char* dstRegName = NULL;
    codeGenPrepareRegister(INT_REG, dstRegIndex, 0, 0, &dstRegName);

    char* realInstruction = notRealInstruction;
    int resultOfCompareTrue = 1;
    if(strcmp(notRealInstruction, "c.ne.s") == 0)
    {
        realInstruction = "c.eq.s";
        resultOfCompareTrue = 0;
    }
    else if(strcmp(notRealInstruction, "c.ge.s") == 0)
    {
        realInstruction = "c.lt.s";
        resultOfCompareTrue = 0;
    }
    else if(strcmp(notRealInstruction, "c.gt.s") == 0)
    {
        realInstruction = "c.le.s";
        resultOfCompareTrue = 0;
    }
    
    int tmpLabelIndex = getLabelNumber();
    fprintf(g_codeGenOutputFp, "%s %s, %s\n", realInstruction, srcReg1Name, srcReg2Name);
    fprintf(g_codeGenOutputFp, "bc1f _compareFalse_%d\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "li %s, %d\n", dstRegName, resultOfCompareTrue);
    fprintf(g_codeGenOutputFp, "j _compareEnd_%d\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "_compareFalse_%d:\n", tmpLabelIndex);
    fprintf(g_codeGenOutputFp, "li %s, %d\n", dstRegName, !resultOfCompareTrue);
    fprintf(g_codeGenOutputFp, "_compareEnd_%d:\n", tmpLabelIndex);
    
    codeGenSaveToMemoryIfPsuedoRegister(INT_REG, dstRegIndex, dstRegName);
}


void codeGenLogicalInstruction(ProcessorType processorType, char *instruction, int dstRegIndex, int srcReg1Index, int srcReg2Index)
{
    int boolReg1Index = -1;
    int boolReg2Index = -1;

    if(processorType == FLOAT_REG)
    {
        boolReg1Index = getRegister(INT_REG);
        boolReg2Index = getRegister(INT_REG);
        codeGenGetBoolOfFloat(boolReg1Index, srcReg1Index);
        codeGenGetBoolOfFloat(boolReg2Index, srcReg2Index);
    }
    else if(processorType == INT_REG)
    {
        int zero = 0;
        boolReg1Index = srcReg1Index;
        boolReg2Index = srcReg2Index;
        codeGen2Reg1ImmInstruction(INT_REG, "sne", boolReg1Index, srcReg1Index, &zero);
        codeGen2Reg1ImmInstruction(INT_REG, "sne", boolReg2Index, srcReg2Index, &zero);
    }

    codeGen3RegInstruction(INT_REG, instruction, dstRegIndex, boolReg1Index, boolReg2Index);
    
    if(processorType == FLOAT_REG)
    {
        freeRegister(INT_REG, boolReg1Index);
        freeRegister(INT_REG, boolReg2Index);
    }
}


void codeGen2RegInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index)
{
    char* reg1Name = NULL;
    codeGenPrepareRegister(processorType, reg1Index, 0, 0, &reg1Name);
    
    char* reg2Name = NULL;
    codeGenPrepareRegister(processorType, reg2Index, 1, 1, &reg2Name);
    
    fprintf(g_codeGenOutputFp, "%s %s, %s\n", instruction, reg1Name, reg2Name);

    codeGenSaveToMemoryIfPsuedoRegister(processorType, reg1Index, reg1Name);
}


void codeGen3RegInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index, int reg3Index)
{
    char* reg1Name = NULL;
    codeGenPrepareRegister(processorType, reg1Index, 0, 0, &reg1Name);
    
    char* reg2Name = NULL;
    codeGenPrepareRegister(processorType, reg2Index, 1, 0, &reg2Name);
    
    char* reg3Name = NULL;
    codeGenPrepareRegister(processorType, reg3Index, 1, 1, &reg3Name);
    
    fprintf(g_codeGenOutputFp, "%s %s, %s, %s\n", instruction, reg1Name, reg2Name, reg3Name);

    codeGenSaveToMemoryIfPsuedoRegister(processorType, reg1Index, reg1Name);
}


void codeGen2Reg1ImmInstruction(ProcessorType processorType, char* instruction, int reg1Index, int reg2Index, void* imm)
{
    char* reg1Name = NULL;
    codeGenPrepareRegister(processorType, reg1Index, 0, 0, &reg1Name);
    
    char* reg2Name = NULL;
    codeGenPrepareRegister(processorType, reg2Index, 1, 0, &reg2Name);
    
    if(processorType == INT_REG)
    {
        int* val = (int*)imm;
        fprintf(g_codeGenOutputFp, "%s %s, %s, %d\n", instruction, reg1Name, reg2Name, *val);
    }
    else if(processorType == FLOAT_REG)
    {
        float* val = (float*)imm;
        fprintf(g_codeGenOutputFp, "%s %s, %s, %f\n", instruction, reg1Name, reg2Name, *val);
    }

    codeGenSaveToMemoryIfPsuedoRegister(processorType, reg1Index, reg1Name);
}


int codeGenConvertFromIntToFloat(int intRegIndex)
{
    /* TODO: Check correctness */
    int floatRegIndex = getRegister(FLOAT_REG);

    char* Rsrc = NULL;
    codeGenPrepareRegister(INT_REG, intRegIndex, 1, 0, &Rsrc);
    char* CPdest = NULL;
    codeGenPrepareRegister(FLOAT_REG, floatRegIndex, 0, 0, &CPdest);

    fprintf(g_codeGenOutputFp, "mtc1 %s, %s\n", Rsrc, CPdest);
    fprintf(g_codeGenOutputFp, "cvt.s.w %s, %s\n", CPdest, CPdest);
    freeRegister(INT_REG, intRegIndex);
    codeGenSaveToMemoryIfPsuedoRegister(FLOAT_REG, floatRegIndex, CPdest);

    return floatRegIndex;
}


int codeGenConvertFromFloatToInt(int floatRegIndex)
{
    /* TODO: Check correctness */
    int intRegIndex = getRegister(INT_REG);

    char* CPsrc = NULL;
    codeGenPrepareRegister(FLOAT_REG, floatRegIndex, 1, 0, &CPsrc);
    char* Rdest = NULL;
    codeGenPrepareRegister(INT_REG, intRegIndex, 0, 0, &Rdest);

    fprintf(g_codeGenOutputFp, "cvt.w.s %s, %s\n", CPsrc, CPsrc);
    fprintf(g_codeGenOutputFp, "mfc1 %s, %s\n", Rdest, CPsrc);
    freeRegister(FLOAT_REG, floatRegIndex);
    codeGenSaveToMemoryIfPsuedoRegister(INT_REG, intRegIndex, Rdest);
    
    return intRegIndex;
}


void codeGenerate(AST_NODE *root)
{
    char* outputfileName = "output.s";
    g_codeGenOutputFp = fopen(outputfileName, "w");
    if(!g_codeGenOutputFp)
    {
        printf("Cannot open file \"%s\"", outputfileName);
        exit(EXIT_FAILURE);
    }

    codeGenProgramNode(root);
}

// .data save to data segment
// .text save to text segment
void codeGenProgramNode(AST_NODE *programNode)
{
    AST_NODE *traverseDeclaration = programNode->child;
    while(traverseDeclaration)
    {
        if(traverseDeclaration->nodeType == VARIABLE_DECL_LIST_NODE)
        {
            fprintf(g_codeGenOutputFp, ".data\n");
            codeGenGlobalVariable(traverseDeclaration); // Handle the Global apart
            fprintf(g_codeGenOutputFp, ".text\n");
        }
        else if(traverseDeclaration->nodeType == DECLARATION_NODE)
        {
            codeGenFunctionDeclaration(traverseDeclaration);
        }
        traverseDeclaration = traverseDeclaration->rightSibling;
    }
    return;
}

void getExprOrConstValue_(AST_NODE* exprOrConstNode, int* iValue, float* fValue)
{
	if(exprOrConstNode->nodeType == CONST_VALUE_NODE)
	{
		if(exprOrConstNode->dataType == INT_TYPE)
		{
			if(fValue)
			{
				*fValue = exprOrConstNode->semantic_value.const1->const_u.intval;
			}
			else
			{
				*iValue = exprOrConstNode->semantic_value.const1->const_u.intval;
			}
		}
		else
		{
			*fValue = exprOrConstNode->semantic_value.const1->const_u.fval;
		}
	}
	else
	{
		if(exprOrConstNode->dataType == INT_TYPE)
		{
			if(fValue)
			{
				*fValue = exprOrConstNode->semantic_value.exprSemanticValue.constEvalValue.iValue;
			}
			else
			{
				*iValue = exprOrConstNode->semantic_value.exprSemanticValue.constEvalValue.iValue;
			}
		}
		else
		{
			*fValue = exprOrConstNode->semantic_value.exprSemanticValue.constEvalValue.fValue;
		}
	}
}

// Handle the GlobalVariable directly
// TODO: Need to handle the Initialization [Written]
void codeGenGlobalVariable(AST_NODE* varaibleDeclListNode)
{
    AST_NODE *traverseDeclaration = varaibleDeclListNode->child;
    while(traverseDeclaration)
    {
        if(traverseDeclaration->semantic_value.declSemanticValue.kind == VARIABLE_DECL)
        {
            AST_NODE *idNode = traverseDeclaration->child->rightSibling;
            while(idNode)
            {
                SymbolTableEntry* idSymbolTableEntry = idNode->semantic_value.identifierSemanticValue.symbolTableEntry;
                TypeDescriptor* idTypeDescriptor = idSymbolTableEntry->attribute->attr.typeDescriptor;
                if(idTypeDescriptor->kind == SCALAR_TYPE_DESCRIPTOR)
                {
                    if(idTypeDescriptor->properties.dataType == INT_TYPE)
                    {
						// TODO: Check Initialization [Written]
						if (idNode->semantic_value.identifierSemanticValue.kind == NORMAL_ID)
                        	fprintf(g_codeGenOutputFp, "_g_%s: .word 0\n", idSymbolTableEntry->name);
						else {
							int value;
							getExprOrConstValue_(idNode->child, &value, NULL);
                        	fprintf(g_codeGenOutputFp, "_g_%s: .word %d\n", idSymbolTableEntry->name, value);
						}
                    }
                    else if(idTypeDescriptor->properties.dataType == FLOAT_TYPE)
                    {
						// TODO: Check Initialization [Written]
						if (idNode->semantic_value.identifierSemanticValue.kind == NORMAL_ID)
                        	fprintf(g_codeGenOutputFp, "_g_%s: .float 0.0\n", idSymbolTableEntry->name);
						else {
							float value;
							getExprOrConstValue_(idNode->child, NULL, &value);
                        	fprintf(g_codeGenOutputFp, "_g_%s: .float %f\n", idSymbolTableEntry->name, value);
						}
                    }
                }
                else if(idTypeDescriptor->kind == ARRAY_TYPE_DESCRIPTOR)
                {
                    int variableSize = getVariableSize(idTypeDescriptor);
                    fprintf(g_codeGenOutputFp, "_g_%s: .space %d\n", idSymbolTableEntry->name, variableSize);
                }
                idNode = idNode->rightSibling;
            }
        }
        traverseDeclaration = traverseDeclaration->rightSibling;
    }
    return;
}

// For Local variable initialization
void codeGenLocalVariableInitialization(AST_NODE *varaibleDeclListNode) {
    
	AST_NODE *traverseDeclaration = varaibleDeclListNode->child;
    while(traverseDeclaration)
    {
        if(traverseDeclaration->semantic_value.declSemanticValue.kind == VARIABLE_DECL)
        {
            AST_NODE *idNode = traverseDeclaration->child->rightSibling;
            while(idNode)
            {
                SymbolTableEntry* idSymbolTableEntry = idNode->semantic_value.identifierSemanticValue.symbolTableEntry;
                TypeDescriptor* idTypeDescriptor = idSymbolTableEntry->attribute->attr.typeDescriptor;
                if(idTypeDescriptor->kind == SCALAR_TYPE_DESCRIPTOR)
                {
                    if(idTypeDescriptor->properties.dataType == INT_TYPE)
                    {
						// TODO: Code Initialization [Written]
						if (idNode->semantic_value.identifierSemanticValue.kind == WITH_INIT_ID) {
							AST_NODE * rightOp = idNode->child;
							codeGenExprRelatedNode(rightOp);
							if (rightOp->dataType == FLOAT_TYPE)
				        		rightOp->registerIndex = codeGenConvertFromFloatToInt(rightOp->registerIndex);
			            	char* rightOpRegName = NULL;
            				codeGenPrepareRegister(INT_REG, rightOp->registerIndex, 1, 0, &rightOpRegName);
                			fprintf(g_codeGenOutputFp, "sw %s, %d($fp)\n", rightOpRegName, idSymbolTableEntry->attribute->offsetInAR);
            				idNode->registerIndex = rightOp->registerIndex;
						}
                    }
                    else if(idTypeDescriptor->properties.dataType == FLOAT_TYPE)
                    {
						// TODO: Code Initialization [Written]
						if (idNode->semantic_value.identifierSemanticValue.kind == WITH_INIT_ID) {
							AST_NODE * rightOp = idNode->child;
							codeGenExprRelatedNode(rightOp);
							if (rightOp->dataType == INT_TYPE)
				            	rightOp->registerIndex = codeGenConvertFromIntToFloat(rightOp->registerIndex);
			            	char* rightOpRegName = NULL;
            				codeGenPrepareRegister(FLOAT_REG, rightOp->registerIndex, 1, 0, &rightOpRegName);
                			fprintf(g_codeGenOutputFp, "s.s %s, %d($fp)\n", rightOpRegName, idSymbolTableEntry->attribute->offsetInAR);
            				idNode->registerIndex = rightOp->registerIndex;
						}
                    }
                }
                else if(idTypeDescriptor->kind == ARRAY_TYPE_DESCRIPTOR)
                {
					// Can't be initialized !
                }
                idNode = idNode->rightSibling;
            }
        }
        traverseDeclaration = traverseDeclaration->rightSibling;
    }
    return;

}

void setOffsetsOfParameters(AST_NODE * parameterNode) {
	AST_NODE * child = parameterNode->child; //FUNCTION_PARAMETER_DECL
	int param_Offset = 8;
	while (child != NULL) {
		AST_NODE * idNode = child->child->rightSibling;
        SymbolTableEntry* idSymbolTableEntry = idNode->semantic_value.identifierSemanticValue.symbolTableEntry;
        TypeDescriptor* idTypeDescriptor = idSymbolTableEntry->attribute->attr.typeDescriptor;
		if (idTypeDescriptor->kind == SCALAR_TYPE_DESCRIPTOR) {
			idSymbolTableEntry->attribute->offsetInAR = param_Offset;
			// printf ("ID = %s param_Offset = %d\n", idNode->semantic_value.identifierSemanticValue.identifierName, param_Offset);
		}
		else if (idTypeDescriptor->kind == ARRAY_TYPE_DESCRIPTOR) {
			// TODO: Handle the array pointer [Written] ---> the same as SCALAR_TYPE_DESCRIPTOR	
			// TODO: Need to change the codeGenCalcArrayElemenetAddress
			idSymbolTableEntry->attribute->offsetInAR = param_Offset;
		}
		param_Offset += 4;
		child = child->rightSibling;
	}
}

void codeGenFunctionDeclaration(AST_NODE *functionDeclNode)
{
    AST_NODE* functionIdNode = functionDeclNode->child->rightSibling;
    
    g_currentFunctionName = functionIdNode->semantic_value.identifierSemanticValue.identifierName;

    fprintf(g_codeGenOutputFp, ".text\n");
    if (strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "main") != 0) {
        fprintf(g_codeGenOutputFp, "_start_%s:\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName);
    } else {
        fprintf(g_codeGenOutputFp, "%s:\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName);
    }
   	// TODO: load parameter from stack to $a0 ~ $a3 [Written]
    fprintf(g_codeGenOutputFp, "lw $a0, 4($sp)\n");
    fprintf(g_codeGenOutputFp, "lw $a1, 8($sp)\n");
    fprintf(g_codeGenOutputFp, "lw $a2, 12($sp)\n");
    fprintf(g_codeGenOutputFp, "lw $a3, 16($sp)\n");
    //prologue
    fprintf(g_codeGenOutputFp, "sw $ra, 0($sp)\n");
    fprintf(g_codeGenOutputFp, "sw $fp, -4($sp)\n");
    fprintf(g_codeGenOutputFp, "add $fp, $sp, -4\n");
    fprintf(g_codeGenOutputFp, "add $sp, $sp, -8\n");
    fprintf(g_codeGenOutputFp, "lw $2, _frameSize_%s\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName);
    fprintf(g_codeGenOutputFp, "sub $sp, $sp, $2\n");
    printStoreRegister(g_codeGenOutputFp);

    resetRegisterTable(functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->offsetInAR);
	AST_NODE* parameterNode = functionIdNode->rightSibling;
	// TODO: set the offset of the parameters [Written]
	setOffsetsOfParameters(parameterNode);
    AST_NODE* blockNode = functionIdNode->rightSibling->rightSibling;
    AST_NODE *traverseListNode = blockNode->child;
    while(traverseListNode)
    {
        codeGenGeneralNode(traverseListNode);
        traverseListNode = traverseListNode->rightSibling;
    }

    //epilogue
    fprintf(g_codeGenOutputFp, "_end_%s:\n", g_currentFunctionName);
    printRestoreRegister(g_codeGenOutputFp);
    fprintf(g_codeGenOutputFp, "lw $ra, 4($fp)\n");
    fprintf(g_codeGenOutputFp, "add $sp, $fp, 4\n");
    fprintf(g_codeGenOutputFp, "lw $fp, 0($fp)\n");
    if (strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "main") == 0)
    {
        fprintf(g_codeGenOutputFp, "li $v0, 10\n");
        fprintf(g_codeGenOutputFp, "syscall\n");
    }
    else
    {
        fprintf(g_codeGenOutputFp, "jr $ra\n");
    }
    fprintf(g_codeGenOutputFp, ".data\n");

    int frameSize = abs(functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->offsetInAR) + 
        (INT_REGISTER_COUNT + INT_WORK_REGISTER_COUNT + FLOAT_REGISTER_COUNT + FLOAT_WORK_REGISTER_COUNT) * 4 +
        g_pseudoRegisterTable.isAllocatedVector->size * 4;
    fprintf(g_codeGenOutputFp, "_frameSize_%s: .word %d\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName, frameSize);
    return;
}


void codeGenBlockNode(AST_NODE* blockNode)
{
    AST_NODE *traverseListNode = blockNode->child;
    while(traverseListNode)
    {
        codeGenGeneralNode(traverseListNode);
        traverseListNode = traverseListNode->rightSibling;
    }
}


void codeGenExprNode(AST_NODE* exprNode)
{
    if(exprNode->semantic_value.exprSemanticValue.kind == BINARY_OPERATION)
    {
        AST_NODE* leftOp = exprNode->child;
        AST_NODE* rightOp = leftOp->rightSibling;
        codeGenExprRelatedNode(leftOp);

        /* Short circuit evaluation */
        int labelNumber = -1;
        if(exprNode->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_AND)
        {
            labelNumber = getLabelNumber();
            char* boolRegName = NULL;
            if(leftOp->dataType == INT_TYPE)
            {
                codeGenPrepareRegister(INT_REG, leftOp->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "beqz %s _shortCircuitANDLabel_%d\n", boolRegName, labelNumber);
            }
            else if(leftOp->dataType == FLOAT_TYPE)
            {
                float zero = 0.0f;
                int constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);
                fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", floatWorkRegisterName[0], constantZeroLabelNumber);
                codeGenPrepareRegister(FLOAT_REG, leftOp->registerIndex, 1, 1, &boolRegName);
                fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", boolRegName, floatWorkRegisterName[0]);
                fprintf(g_codeGenOutputFp, "bc1t _shortCircuitANDLabel_%d\n", labelNumber);
            }
        }
        else if(exprNode->semantic_value.exprSemanticValue.op.binaryOp == BINARY_OP_OR)
        {
            labelNumber = getLabelNumber();
            char* boolRegName = NULL;
            if(leftOp->dataType == INT_TYPE)
            {
                codeGenPrepareRegister(INT_REG, leftOp->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "bnez %s _shortCircuitORLabel_%d\n", boolRegName, labelNumber);
            }
            else if(leftOp->dataType == FLOAT_TYPE)
            {
                float zero = 0.0f;
                int constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);
                fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", floatWorkRegisterName[0], constantZeroLabelNumber);
                codeGenPrepareRegister(FLOAT_REG, leftOp->registerIndex, 1, 1, &boolRegName);
                fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", boolRegName, floatWorkRegisterName[0]);
                fprintf(g_codeGenOutputFp, "bc1f _shortCircuitORLabel_%d\n", labelNumber);
            }
        }

        codeGenExprRelatedNode(rightOp);
        char* boolRegName = NULL;
        if(leftOp->dataType == FLOAT_TYPE || rightOp->dataType == FLOAT_TYPE)
        {
            if(leftOp->dataType == INT_TYPE)
            {
                leftOp->registerIndex = codeGenConvertFromIntToFloat(leftOp->registerIndex);
            }
            //else if
            if(rightOp->dataType == INT_TYPE)
            {
                rightOp->registerIndex = codeGenConvertFromIntToFloat(rightOp->registerIndex);
            }
            
            switch(exprNode->semantic_value.exprSemanticValue.op.binaryOp)
            {
            case BINARY_OP_ADD:
                exprNode->registerIndex = leftOp->registerIndex;
                codeGen3RegInstruction(FLOAT_REG, "add.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_SUB:
                exprNode->registerIndex = leftOp->registerIndex;
                codeGen3RegInstruction(FLOAT_REG, "sub.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_MUL:
                exprNode->registerIndex = leftOp->registerIndex;
                codeGen3RegInstruction(FLOAT_REG, "mul.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_DIV:
                exprNode->registerIndex = leftOp->registerIndex;
                codeGen3RegInstruction(FLOAT_REG, "div.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_EQ:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.eq.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_GE:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.ge.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_LE:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.le.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_NE:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.ne.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_GT:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.gt.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_LT:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenFloatCompInstruction("c.lt.s", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_AND:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenLogicalInstruction(FLOAT_REG, "and", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                fprintf(g_codeGenOutputFp, "j _shortCircuitANDExit_%d\n", labelNumber);
                fprintf(g_codeGenOutputFp, "_shortCircuitANDLabel_%d:\n", labelNumber);
                codeGenPrepareRegister(INT_REG, exprNode->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "addi %s, $0, 0\n", boolRegName);
                fprintf(g_codeGenOutputFp, "_shortCircuitANDExit_%d:\n", labelNumber);
                codeGenSaveToMemoryIfPsuedoRegister(INT_REG, exprNode->registerIndex, boolRegName);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            case BINARY_OP_OR:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenLogicalInstruction(FLOAT_REG, "or", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                fprintf(g_codeGenOutputFp, "j _shortCircuitORExit_%d\n", labelNumber);
                fprintf(g_codeGenOutputFp, "_shortCircuitORLabel_%d:\n", labelNumber);
                codeGenPrepareRegister(INT_REG, exprNode->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "addi %s, $0, 1\n", boolRegName);
                fprintf(g_codeGenOutputFp, "_shortCircuitORExit_%d:\n", labelNumber);
                codeGenSaveToMemoryIfPsuedoRegister(INT_REG, exprNode->registerIndex, boolRegName);
                freeRegister(FLOAT_REG, leftOp->registerIndex);
                break;
            default:
                printf("Unhandled case in void evaluateExprValue(AST_NODE* exprNode)\n");
                break;
            }

            freeRegister(FLOAT_REG, rightOp->registerIndex);
        }//endif at least one float operand
        else if(exprNode->dataType == INT_TYPE)
        {
            exprNode->registerIndex = leftOp->registerIndex;
            switch(exprNode->semantic_value.exprSemanticValue.op.binaryOp)
            {
            case BINARY_OP_ADD:
                codeGen3RegInstruction(INT_REG, "add", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_SUB:
                codeGen3RegInstruction(INT_REG, "sub", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_MUL:
                codeGen3RegInstruction(INT_REG, "mul", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_DIV:
                codeGen3RegInstruction(INT_REG, "div", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_EQ:
                codeGen3RegInstruction(INT_REG, "seq", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_GE:
                codeGen3RegInstruction(INT_REG, "sge", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_LE:
                codeGen3RegInstruction(INT_REG, "sle", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_NE:
                codeGen3RegInstruction(INT_REG, "sne", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_GT:
                codeGen3RegInstruction(INT_REG, "sgt", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_LT:
                codeGen3RegInstruction(INT_REG, "slt", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                break;
            case BINARY_OP_AND:
                codeGenLogicalInstruction(INT_REG, "and", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                fprintf(g_codeGenOutputFp, "j _shortCircuitANDExit_%d\n", labelNumber);
                fprintf(g_codeGenOutputFp, "_shortCircuitANDLabel_%d:\n", labelNumber);
                codeGenPrepareRegister(INT_REG, exprNode->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "addi %s, $0, 0\n", boolRegName);
                fprintf(g_codeGenOutputFp, "_shortCircuitANDExit_%d:\n", labelNumber);
                codeGenSaveToMemoryIfPsuedoRegister(INT_REG, exprNode->registerIndex, boolRegName);
                break;
            case BINARY_OP_OR:
                codeGenLogicalInstruction(INT_REG, "or", exprNode->registerIndex, leftOp->registerIndex, rightOp->registerIndex);
                fprintf(g_codeGenOutputFp, "j _shortCircuitORExit_%d\n", labelNumber);
                fprintf(g_codeGenOutputFp, "_shortCircuitORLabel_%d:\n", labelNumber);
                codeGenPrepareRegister(INT_REG, exprNode->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "addi %s, $0, 1\n", boolRegName);
                fprintf(g_codeGenOutputFp, "_shortCircuitORExit_%d:\n", labelNumber);
                codeGenSaveToMemoryIfPsuedoRegister(INT_REG, exprNode->registerIndex, boolRegName);
                break;
            default:
                printf("Unhandled case in void evaluateExprValue(AST_NODE* exprNode)\n");
                break;
            }

            freeRegister(INT_REG, rightOp->registerIndex);
        }//endif 2 int operands
    }//endif BINARY_OPERATION
    else if(exprNode->semantic_value.exprSemanticValue.kind == UNARY_OPERATION)
    {
        int tmpZero = 0;
        AST_NODE* operand = exprNode->child;
        codeGenExprRelatedNode(operand);
        if(operand->dataType == FLOAT_TYPE)
        {
            switch(exprNode->semantic_value.exprSemanticValue.op.unaryOp)
            {
            case UNARY_OP_POSITIVE:
                exprNode->registerIndex = operand->registerIndex;
                break;
            case UNARY_OP_NEGATIVE:
                exprNode->registerIndex = operand->registerIndex;
                codeGen2RegInstruction(FLOAT_REG, "neg.s", exprNode->registerIndex, exprNode->registerIndex);
                break;
            case UNARY_OP_LOGICAL_NEGATION:
                exprNode->registerIndex = getRegister(INT_REG);
                codeGenGetBoolOfFloat(exprNode->registerIndex, operand->registerIndex);
                codeGen2Reg1ImmInstruction(INT_REG, "seq", exprNode->registerIndex, exprNode->registerIndex, &tmpZero);
                freeRegister(FLOAT_REG, operand->registerIndex);
                break;
            default:
                printf("Unhandled case in void evaluateExprValue(AST_NODE* exprNode)\n");
                break;
            }
        }
        else if(operand->dataType == INT_TYPE)
        {
            switch(exprNode->semantic_value.exprSemanticValue.op.unaryOp)
            {
            case UNARY_OP_POSITIVE:
                exprNode->registerIndex = operand->registerIndex;
                break;
            case UNARY_OP_NEGATIVE:
                exprNode->registerIndex = operand->registerIndex;
                codeGen2RegInstruction(INT_REG, "neg", exprNode->registerIndex, exprNode->registerIndex);
                break;
            case UNARY_OP_LOGICAL_NEGATION:
                exprNode->registerIndex = operand->registerIndex;
                codeGen2Reg1ImmInstruction(INT_REG, "seq", exprNode->registerIndex, exprNode->registerIndex, &tmpZero);
                break;
            default:
                printf("Unhandled case in void evaluateExprValue(AST_NODE* exprNode)\n");
                break;
            }
        }
    }
}


void codeGenFunctionCall(AST_NODE* functionCallNode)
{
    AST_NODE* functionIdNode = functionCallNode->child;
    AST_NODE* parameterList = functionIdNode->rightSibling;
    if(strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "write") == 0)
    {
        AST_NODE* firstParameter = parameterList->child;
        codeGenExprRelatedNode(firstParameter);
        char* parameterRegName = NULL;
        switch(firstParameter->dataType)
        {
        case INT_TYPE:
            fprintf(g_codeGenOutputFp, "li $v0, 1\n");
            codeGenPrepareRegister(INT_REG, firstParameter->registerIndex, 1, 0, &parameterRegName);
            fprintf(g_codeGenOutputFp, "move $a0, %s\n", parameterRegName);
            freeRegister(INT_REG, firstParameter->registerIndex);
            break;
        case FLOAT_TYPE:
            fprintf(g_codeGenOutputFp, "li $v0, 2\n");
            codeGenPrepareRegister(FLOAT_REG, firstParameter->registerIndex, 1, 0, &parameterRegName);
            fprintf(g_codeGenOutputFp, "mov.s $f12, %s\n", parameterRegName);
            freeRegister(FLOAT_REG, firstParameter->registerIndex);
            break;
        case CONST_STRING_TYPE:
            fprintf(g_codeGenOutputFp, "li $v0, 4\n");
            codeGenPrepareRegister(INT_REG, firstParameter->registerIndex, 1, 0, &parameterRegName);
            fprintf(g_codeGenOutputFp, "move $a0, %s\n", parameterRegName);
            freeRegister(INT_REG, firstParameter->registerIndex);
            break;
        default:
            printf("Unhandled case in void codeGenFunctionCall(AST_NODE* functionCallNode)\n");
            printf("firstParameter->registerIndex was not free\n");
            break;
        }
        fprintf(g_codeGenOutputFp, "syscall\n");
        return;
    }


    if(strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "read") == 0)
    {
        fprintf(g_codeGenOutputFp, "li $v0, 5\n");
        fprintf(g_codeGenOutputFp, "syscall\n");
    }
    else if(strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "fread") == 0)
    {
        fprintf(g_codeGenOutputFp, "li $v0, 6\n");
        fprintf(g_codeGenOutputFp, "syscall\n");
    }
    else
    {
		// TODO: Parameter passing [Written]
		int passingNumber = 0;
		if (functionIdNode->rightSibling->nodeType == NONEMPTY_RELOP_EXPR_LIST_NODE) {
			AST_NODE * passingNode = functionIdNode->rightSibling->child;
			SymbolTableEntry* functionIdNodeEntry = functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry;
			FunctionSignature* signature = functionIdNodeEntry->attribute->attr.functionSignature;
			int parameter_counter = 4;
			passingNumber = signature->parametersCount;
            fprintf(g_codeGenOutputFp, "add $sp, $sp, -%d\n", 4 * passingNumber);	
			Parameter* realList = signature->parameterList;	
			
			while (passingNode != NULL) {
				codeGenExprRelatedNode(passingNode);
        		TypeDescriptor* realTypeDescriptor = realList->type;
				
				// SCALAR_TYPE_DESCRIPTOR or ARRAY_TYPE_DESCRIPTOR
				if (realTypeDescriptor->kind == SCALAR_TYPE_DESCRIPTOR) {
					// TODO: Handle the type conversion [Written]
					DATA_TYPE realType = realTypeDescriptor->properties.dataType;	
					if (realType == INT_TYPE) {
						if (passingNode->dataType == FLOAT_TYPE)
							passingNode->registerIndex = codeGenConvertFromFloatToInt(passingNode->registerIndex);
			            char* exprRegName = NULL;
            			codeGenPrepareRegister(INT_REG, passingNode->registerIndex, 1, 0, &exprRegName);
                		fprintf(g_codeGenOutputFp, "sw %s, %d($sp)\n", exprRegName, parameter_counter);
					}
					else if (realType == FLOAT_TYPE) {
						if (passingNode->dataType == INT_TYPE)
							passingNode->registerIndex = codeGenConvertFromIntToFloat(passingNode->registerIndex);
			            char* exprRegName = NULL;
            			codeGenPrepareRegister(FLOAT_REG, passingNode->registerIndex, 1, 0, &exprRegName);
                		fprintf(g_codeGenOutputFp, "s.s %s, %d($sp)\n", exprRegName, parameter_counter);
					}
				}
				else if (realTypeDescriptor->kind == ARRAY_TYPE_DESCRIPTOR) {
					// TODO: Handle the array ---> push the array address to the stack [Written]
					// Use codeGenCalcArrayElemenetAddress 
					/*
        			SymbolTableEntry* passingSymbolTableEntry = passingNode->semantic_value.identifierSemanticValue.symbolTableEntry;
        			int baseOffset = passingSymbolTableEntry->attribute->offsetInAR;
					if (baseOffset <= 0) {
						char* temp_RegisterName = NULL;
						int temp_RegisterIndex = getRegister(INT_REG);
        				codeGenPrepareRegister(INT_REG, temp_RegisterIndex, 0, 0, &temp_RegisterName);
        				fprintf(g_codeGenOutputFp, "add %s, $fp, %d\n", temp_RegisterName, baseOffset);
                		fprintf(g_codeGenOutputFp, "sw %s, %d($sp)\n", temp_RegisterName, parameter_counter);
						freeRegister(INT_REG, temp_RegisterIndex);
					} else { //parameter reference
						//lw the offset first
						int offsetRegisterIndex = getRegister(INT_REG);
						char * offsetRegisterName = NULL;
            			codeGenPrepareRegister(INT_REG, offsetRegisterIndex, 0, 0, &offsetRegisterName);
            			fprintf(g_codeGenOutputFp, "lw %s, %d($fp)\n", offsetRegisterName, baseOffset);
                		fprintf(g_codeGenOutputFp, "sw %s, %d($sp)\n", offsetRegisterName, parameter_counter);
						freeRegister(INT_REG, offsetRegisterIndex);
					}*/
					int addressRegisterIndex = codeGenCalcArrayElemenetAddress(passingNode);
					char* addressRegisterName = NULL;
            		codeGenPrepareRegister(INT_REG, addressRegisterIndex, 0, 0, &addressRegisterName);
                	fprintf(g_codeGenOutputFp, "sw %s, %d($sp)\n", addressRegisterName, parameter_counter);
				}
				parameter_counter += 4;
				passingNode = passingNode->rightSibling;
				realList = realList->next;
			}
		}	
        if (strcmp(functionIdNode->semantic_value.identifierSemanticValue.identifierName, "main") != 0) {
            fprintf(g_codeGenOutputFp, "jal _start_%s\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName);
        } else {
            fprintf(g_codeGenOutputFp, "jal %s\n", functionIdNode->semantic_value.identifierSemanticValue.identifierName);
        }

		if (passingNumber > 0) {
            fprintf(g_codeGenOutputFp, "add $sp, $sp, %d\n", 4 * passingNumber);	
		}
    }




    if (functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry) {
        if(functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType == INT_TYPE)
        {
            functionCallNode->registerIndex = getRegister(INT_REG);
            char* returnIntRegName = NULL;
            codeGenPrepareRegister(INT_REG, functionCallNode->registerIndex, 0, 0, &returnIntRegName);

            fprintf(g_codeGenOutputFp, "move %s, $v0\n", returnIntRegName);

            codeGenSaveToMemoryIfPsuedoRegister(INT_REG, functionCallNode->registerIndex, returnIntRegName);
        }
        else if(functionIdNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.functionSignature->returnType == FLOAT_TYPE)
        {
            functionCallNode->registerIndex = getRegister(FLOAT_REG);
            char* returnfloatRegName = NULL;
            codeGenPrepareRegister(FLOAT_REG, functionCallNode->registerIndex, 0, 0, &returnfloatRegName);

            fprintf(g_codeGenOutputFp, "mov.s %s, $f0\n", returnfloatRegName);

            codeGenSaveToMemoryIfPsuedoRegister(FLOAT_REG, functionCallNode->registerIndex, returnfloatRegName);
        }
    }
}


int codeGenCalcArrayElemenetAddress(AST_NODE* idNode)
{
    AST_NODE* traverseDim = idNode->child;
    int* sizeInEachDimension = idNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.arrayProperties.sizeInEachDimension;
    int dimensions = idNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->attr.typeDescriptor->properties.arrayProperties.dimension;
    // TODO: multiple dimensions [Written]
	int dimIndex = 1;
	int linearIdxRegisterIndex, secondIndex;
	if (traverseDim != NULL) {
    	codeGenExprRelatedNode(traverseDim);
		linearIdxRegisterIndex = traverseDim->registerIndex;

    	traverseDim = traverseDim->rightSibling;
		int dimNum;
		while (traverseDim != NULL) {
			codeGenExprRelatedNode(traverseDim);
			secondIndex = traverseDim->registerIndex;
			dimNum = sizeInEachDimension[dimIndex++];
			codeGen2Reg1ImmInstruction(INT_REG, "mul", linearIdxRegisterIndex, linearIdxRegisterIndex, &dimNum);
			codeGen3RegInstruction(INT_REG, "add", linearIdxRegisterIndex, linearIdxRegisterIndex, secondIndex);
			freeRegister(INT_REG, secondIndex);
			// TODO: Free the secondIndex [Written] 
			traverseDim = traverseDim->rightSibling;
		}
		// Compute The remaining offset
		while (dimIndex < dimensions) {
			dimNum = sizeInEachDimension[dimIndex++];
			codeGen2Reg1ImmInstruction(INT_REG, "mul", linearIdxRegisterIndex, linearIdxRegisterIndex, &dimNum);	
		}
	}
	else {
    	char* linearOffsetRegName = NULL;
		linearIdxRegisterIndex = getRegister(INT_REG);
        codeGenPrepareRegister(INT_REG, linearIdxRegisterIndex, 0, 0, &linearOffsetRegName);
        fprintf(g_codeGenOutputFp, "li %s, 0\n", linearOffsetRegName);
	}

    
    int shiftLeftTwoBits = 2;
    codeGen2Reg1ImmInstruction(INT_REG, "sll", linearIdxRegisterIndex, linearIdxRegisterIndex, &shiftLeftTwoBits);
    
    char* linearOffsetRegName = NULL;
    if(!isGlobalVariable(idNode->semantic_value.identifierSemanticValue.symbolTableEntry))
    {
        int baseOffset = idNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->offsetInAR;
		if (baseOffset <= 0) {
        	codeGen2Reg1ImmInstruction(INT_REG, "add", linearIdxRegisterIndex, linearIdxRegisterIndex, &baseOffset);
        	codeGenPrepareRegister(INT_REG, linearIdxRegisterIndex, 1, 0, &linearOffsetRegName);
        	fprintf(g_codeGenOutputFp, "add %s, %s, $fp\n", linearOffsetRegName, linearOffsetRegName);
		} else { // parameter reference
			// lw the offset first
			int offsetRegisterIndex = getRegister(INT_REG);
			char * offsetRegisterName = NULL;
            codeGenPrepareRegister(INT_REG, offsetRegisterIndex, 0, 0, &offsetRegisterName);
        	codeGenPrepareRegister(INT_REG, linearIdxRegisterIndex, 1, 0, &linearOffsetRegName);	
            fprintf(g_codeGenOutputFp, "lw %s, %d($fp)\n", offsetRegisterName, baseOffset);
        	fprintf(g_codeGenOutputFp, "add %s, %s, %s\n", linearOffsetRegName, linearOffsetRegName, offsetRegisterName);
			freeRegister(INT_REG, offsetRegisterIndex);
		}
    }
    else
    {
        fprintf(g_codeGenOutputFp, "la %s, _g_%s\n", intWorkRegisterName[0], idNode->semantic_value.identifierSemanticValue.identifierName);
        codeGenPrepareRegister(INT_REG, linearIdxRegisterIndex, 1, 1, &linearOffsetRegName);
        fprintf(g_codeGenOutputFp, "add %s, %s, %s\n", linearOffsetRegName, linearOffsetRegName, intWorkRegisterName[0]);
    }

    codeGenSaveToMemoryIfPsuedoRegister(INT_REG, linearIdxRegisterIndex, linearOffsetRegName);

    return linearIdxRegisterIndex;
}


void codeGenVariableReference(AST_NODE* idNode)
{
    SymbolAttribute *idAttribute = idNode->semantic_value.identifierSemanticValue.symbolTableEntry->attribute;
    if(idNode->semantic_value.identifierSemanticValue.kind == NORMAL_ID)
    {
        if(idNode->dataType == INT_TYPE)
        {
            idNode->registerIndex = getRegister(INT_REG);
            char* loadRegName = NULL;
            if(!isGlobalVariable(idNode->semantic_value.identifierSemanticValue.symbolTableEntry))
            {
                codeGenPrepareRegister(INT_REG, idNode->registerIndex, 0, 0, &loadRegName);
                fprintf(g_codeGenOutputFp, "lw %s, %d($fp)\n", loadRegName, idAttribute->offsetInAR);
            }
            else
            {
                fprintf(g_codeGenOutputFp, "la %s, _g_%s\n", intWorkRegisterName[0], idNode->semantic_value.identifierSemanticValue.identifierName);
                codeGenPrepareRegister(INT_REG, idNode->registerIndex, 0, 1, &loadRegName);
                fprintf(g_codeGenOutputFp, "lw %s, 0(%s)\n", loadRegName, intWorkRegisterName[0]);
            }
            codeGenSaveToMemoryIfPsuedoRegister(INT_REG, idNode->registerIndex, loadRegName);
        }
        else if(idNode->dataType == FLOAT_TYPE)
        {
            idNode->registerIndex = getRegister(FLOAT_REG);
            char* loadRegName = NULL;
            if(!isGlobalVariable(idNode->semantic_value.identifierSemanticValue.symbolTableEntry))
            {
                codeGenPrepareRegister(FLOAT_REG, idNode->registerIndex, 0, 0, &loadRegName);
                fprintf(g_codeGenOutputFp, "l.s %s, %d($fp)\n", loadRegName, idAttribute->offsetInAR);
            }
            else
            {
                fprintf(g_codeGenOutputFp, "la %s, _g_%s\n", intWorkRegisterName[0], idNode->semantic_value.identifierSemanticValue.identifierName);
                codeGenPrepareRegister(FLOAT_REG, idNode->registerIndex, 0, 0, &loadRegName);
                fprintf(g_codeGenOutputFp, "l.s %s, 0(%s)\n", loadRegName, intWorkRegisterName[0]);
            }
            codeGenSaveToMemoryIfPsuedoRegister(FLOAT_REG, idNode->registerIndex, loadRegName);
        }
    }
    else if(idNode->semantic_value.identifierSemanticValue.kind == ARRAY_ID)
    {
        if(idNode->dataType == INT_TYPE || idNode->dataType == FLOAT_TYPE)
        {
            int elementAddressRegIndex = codeGenCalcArrayElemenetAddress(idNode);
            char* elementAddressRegName = NULL;
            codeGenPrepareRegister(INT_REG, elementAddressRegIndex, 1, 0, &elementAddressRegName);
            
            if(idNode->dataType == INT_TYPE)
            {
                idNode->registerIndex = elementAddressRegIndex;
                fprintf(g_codeGenOutputFp, "lw %s, 0(%s)\n", elementAddressRegName, elementAddressRegName);
                codeGenSaveToMemoryIfPsuedoRegister(INT_REG, idNode->registerIndex, elementAddressRegName);
            }
            else if(idNode->dataType == FLOAT_TYPE)
            {
                idNode->registerIndex = getRegister(FLOAT_REG);
                char* dstRegName = NULL;
                codeGenPrepareRegister(FLOAT_REG, idNode->registerIndex, 0, 0, &dstRegName);
                
                char* elementAddressRegName = NULL;
                codeGenPrepareRegister(INT_REG, elementAddressRegIndex, 1, 0, &elementAddressRegName);
            
                fprintf(g_codeGenOutputFp, "l.s %s, 0(%s)\n", dstRegName, elementAddressRegName);
                codeGenSaveToMemoryIfPsuedoRegister(FLOAT_REG, idNode->registerIndex, dstRegName);
            
                freeRegister(INT_REG, elementAddressRegIndex);
            }
        }
    }
}


void codeGenConstantReference(AST_NODE* constantNode)
{
    C_type cType = constantNode->semantic_value.const1->const_type;
    if(cType == INTEGERC)
    {
        int tmpInt = constantNode->semantic_value.const1->const_u.intval;
        int constantLabelNumber = codeGenConstantLabel(INTEGERC, &tmpInt);
        constantNode->registerIndex = getRegister(INT_REG);
        char* regName = NULL;
        codeGenPrepareRegister(INT_REG, constantNode->registerIndex, 0, 0, &regName);
        fprintf(g_codeGenOutputFp, "lw %s, _CONSTANT_%d\n", regName, constantLabelNumber);
        codeGenSaveToMemoryIfPsuedoRegister(INT_REG, constantNode->registerIndex, regName);
    }
    else if(cType == FLOATC)
    {
        float tmpFloat = constantNode->semantic_value.const1->const_u.fval;
        int constantLabelNumber = codeGenConstantLabel(FLOATC, &tmpFloat);
        constantNode->registerIndex = getRegister(FLOAT_REG);
        char* regName = NULL;
        codeGenPrepareRegister(FLOAT_REG, constantNode->registerIndex, 0, 0, &regName);
        fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", regName, constantLabelNumber);
        codeGenSaveToMemoryIfPsuedoRegister(FLOAT_REG, constantNode->registerIndex, regName);
    }
    else if(cType == STRINGC)
    {
        char* tmpCharPtr = constantNode->semantic_value.const1->const_u.sc;
        int constantLabelNumber = codeGenConstantLabel(STRINGC, tmpCharPtr);
        constantNode->registerIndex = getRegister(INT_REG);
        char* regName = NULL;
        codeGenPrepareRegister(INT_REG, constantNode->registerIndex, 0, 0, &regName);
        fprintf(g_codeGenOutputFp, "la %s, _CONSTANT_%d\n", regName, constantLabelNumber);
        codeGenSaveToMemoryIfPsuedoRegister(INT_REG, constantNode->registerIndex, regName);
    }
}


void codeGenExprRelatedNode(AST_NODE* exprRelatedNode)
{
    switch(exprRelatedNode->nodeType)
    {
    case EXPR_NODE:
        codeGenExprNode(exprRelatedNode);
        break;
    case STMT_NODE:
        codeGenFunctionCall(exprRelatedNode);
        break;
    case IDENTIFIER_NODE:
        codeGenVariableReference(exprRelatedNode);
        break;
    case CONST_VALUE_NODE:
        codeGenConstantReference(exprRelatedNode);
        break;
    default:
        printf("Unhandle case in void processExprRelatedNode(AST_NODE* exprRelatedNode)\n");
        exprRelatedNode->dataType = ERROR_TYPE;
        break;
    }
}


void codeGenAssignmentStmt(AST_NODE* assignmentStmtNode)
{
    AST_NODE* leftOp = assignmentStmtNode->child;
    AST_NODE* rightOp = leftOp->rightSibling;
    codeGenExprRelatedNode(rightOp);

    if(leftOp->semantic_value.identifierSemanticValue.kind == NORMAL_ID)
    {
        if(leftOp->dataType == INT_TYPE)
        {
            /* Type conversion */
            if (rightOp->dataType == FLOAT_TYPE)
                rightOp->registerIndex = codeGenConvertFromFloatToInt(rightOp->registerIndex);

            char* rightOpRegName = NULL;
            codeGenPrepareRegister(INT_REG, rightOp->registerIndex, 1, 0, &rightOpRegName);
            if(!isGlobalVariable(leftOp->semantic_value.identifierSemanticValue.symbolTableEntry))
            {
                fprintf(g_codeGenOutputFp, "sw %s, %d($fp)\n", rightOpRegName, leftOp->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->offsetInAR);
            }
            else
            {
                fprintf(g_codeGenOutputFp, "sw %s, _g_%s\n", rightOpRegName, leftOp->semantic_value.identifierSemanticValue.identifierName);
            }
            leftOp->registerIndex = rightOp->registerIndex;
        }
        else if(leftOp->dataType == FLOAT_TYPE)
        {
            /* Type conversion */
            if (rightOp->dataType == INT_TYPE)
                rightOp->registerIndex = codeGenConvertFromIntToFloat(rightOp->registerIndex);

            char* rightOpRegName = NULL;
            codeGenPrepareRegister(FLOAT_REG, rightOp->registerIndex, 1, 0, &rightOpRegName);
            if(!isGlobalVariable(leftOp->semantic_value.identifierSemanticValue.symbolTableEntry))
            {
                fprintf(g_codeGenOutputFp, "s.s %s, %d($fp)\n", rightOpRegName, leftOp->semantic_value.identifierSemanticValue.symbolTableEntry->attribute->offsetInAR);
            }
            else
            {
                fprintf(g_codeGenOutputFp, "s.s %s, _g_%s\n", rightOpRegName, leftOp->semantic_value.identifierSemanticValue.identifierName);
            }
            leftOp->registerIndex = rightOp->registerIndex;
        }
    }
    else if(leftOp->semantic_value.identifierSemanticValue.kind == ARRAY_ID)
    {
        int elementAddressRegIndex = codeGenCalcArrayElemenetAddress(leftOp);

        char* elementAddressRegName = NULL;
        codeGenPrepareRegister(INT_REG, elementAddressRegIndex, 1, 0, &elementAddressRegName);
        if(leftOp->dataType == INT_TYPE)
        {
            /* Type conversion */
            if (rightOp->dataType == FLOAT_TYPE)
                rightOp->registerIndex = codeGenConvertFromFloatToInt(rightOp->registerIndex);

            char* rightOpRegName = NULL;
            codeGenPrepareRegister(INT_REG, rightOp->registerIndex, 1, 1, &rightOpRegName);
            fprintf(g_codeGenOutputFp, "sw %s, 0(%s)\n", rightOpRegName, elementAddressRegName);
            
            leftOp->registerIndex = rightOp->registerIndex;
        }
        else if(leftOp->dataType == FLOAT_TYPE)
        {
            /* Type conversion */
            if (rightOp->dataType == INT_TYPE)
                rightOp->registerIndex = codeGenConvertFromIntToFloat(rightOp->registerIndex);

            char* rightOpRegName = NULL;
            codeGenPrepareRegister(FLOAT_REG, rightOp->registerIndex, 1, 0, &rightOpRegName);
            
            fprintf(g_codeGenOutputFp, "s.s %s, 0(%s)\n", rightOpRegName, elementAddressRegName);

            leftOp->registerIndex = rightOp->registerIndex;
        }

        freeRegister(INT_REG, elementAddressRegIndex);
    }
}


void codeGenAssignOrExpr(AST_NODE* testNode)
{
    if(testNode->nodeType == STMT_NODE)
    {
        if(testNode->semantic_value.stmtSemanticValue.kind == ASSIGN_STMT)
        {
            codeGenAssignmentStmt(testNode);
        }
        else if(testNode->semantic_value.stmtSemanticValue.kind == FUNCTION_CALL_STMT)
        {
            codeGenFunctionCall(testNode);
        }
    }
    else
    {
        codeGenExprRelatedNode(testNode);
    }
}


void codeGenWhileStmt(AST_NODE* whileStmtNode)
{
    AST_NODE* boolExpression = whileStmtNode->child;

    int constantZeroLabelNumber = -1;
    if(boolExpression->dataType == FLOAT_TYPE)
    {
        float zero = 0.0f;
        constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);
    }

    int labelNumber = getLabelNumber();
    fprintf(g_codeGenOutputFp, "_whileTestLabel_%d:\n", labelNumber);
    
    codeGenAssignOrExpr(boolExpression);

    if(boolExpression->dataType == INT_TYPE)
    {
        char* boolRegName = NULL;
        codeGenPrepareRegister(INT_REG, boolExpression->registerIndex, 1, 0, &boolRegName);
        fprintf(g_codeGenOutputFp, "beqz %s, _whileExitLabel_%d\n", boolRegName, labelNumber);
        freeRegister(INT_REG, boolExpression->registerIndex);
    }
    else if(boolExpression->dataType == FLOAT_TYPE)
    {
        fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", floatWorkRegisterName[0], constantZeroLabelNumber);
        char* boolRegName = NULL;
        codeGenPrepareRegister(FLOAT_REG, boolExpression->registerIndex, 1, 1, &boolRegName);
        fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", boolRegName, floatWorkRegisterName[0]);
        fprintf(g_codeGenOutputFp, "bc1t _whileExitLabel_%d\n", labelNumber);
        freeRegister(FLOAT_REG, boolExpression->registerIndex);
    }
    
    AST_NODE* bodyNode = boolExpression->rightSibling;
    codeGenStmtNode(bodyNode);

    fprintf(g_codeGenOutputFp, "j _whileTestLabel_%d\n", labelNumber);
    fprintf(g_codeGenOutputFp, "_whileExitLabel_%d:\n", labelNumber);
}


void codeGenForStmt(AST_NODE* forStmtNode)
{
    /* Check for correctness */
    int labelNumber = getLabelNumber();
    int constantZeroLabelNumber = -1;
    float zero = 0.0f;

    AST_NODE* init = forStmtNode->child;
    AST_NODE* test = init->rightSibling;
    AST_NODE* inc  = test->rightSibling;
    AST_NODE* body = inc->rightSibling;

    codeGenGeneralNode(init);
    /* Gen test */
    fprintf(g_codeGenOutputFp, "_forTestLabel_%d:\n", labelNumber);
    // codeGenGeneralNode(test);
    char *boolRegName = NULL;
    /* Traverse all expressions */
    AST_NODE* traverseNode = test->child;
    while (traverseNode) {
        codeGenExprRelatedNode(traverseNode);
        switch (traverseNode->dataType) {
            case INT_TYPE:
                codeGenPrepareRegister(INT_REG, traverseNode->registerIndex, 1, 0, &boolRegName);
                fprintf(g_codeGenOutputFp, "beqz %s, _forExitLabel_%d\n", boolRegName, labelNumber);
                freeRegister(INT_REG, traverseNode->registerIndex);
                break;
            case FLOAT_TYPE:
                constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);
                fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", floatWorkRegisterName[0], constantZeroLabelNumber);
                codeGenPrepareRegister(FLOAT_REG, traverseNode->registerIndex, 1, 1, &boolRegName);
                fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", boolRegName, floatWorkRegisterName[0]);
                fprintf(g_codeGenOutputFp, "bc1t _forExitLabel_%d\n", labelNumber);
                freeRegister(FLOAT_REG, traverseNode->registerIndex);
                break;
            default:
                fprintf(stderr, "Unhandled data type in codeGenForStmt\n");
                break;
        }
        traverseNode = traverseNode->rightSibling;
    }

    fprintf(g_codeGenOutputFp, "j _forBodyLabel_%d\n", labelNumber);
    /* Gen inc */
    fprintf(g_codeGenOutputFp, "_forIncLabel_%d:\n", labelNumber);
    codeGenGeneralNode(inc);
    fprintf(g_codeGenOutputFp, "j _forTestLabel_%d\n", labelNumber);
    /* Gen body */
    fprintf(g_codeGenOutputFp, "_forBodyLabel_%d:\n", labelNumber);
    codeGenStmtNode(body);
    fprintf(g_codeGenOutputFp, "j _forIncLabel_%d\n", labelNumber);
    fprintf(g_codeGenOutputFp, "_forExitLabel_%d:\n", labelNumber);
}


void codeGenIfStmt(AST_NODE* ifStmtNode)
{
    AST_NODE* boolExpression = ifStmtNode->child;

    int constantZeroLabelNumber = -1;
    if(boolExpression->dataType == FLOAT_TYPE)
    {
        float zero = 0.0f;
        constantZeroLabelNumber = codeGenConstantLabel(FLOATC, &zero);
    }

    int labelNumber = getLabelNumber();

    codeGenAssignOrExpr(boolExpression);

    if(boolExpression->dataType == INT_TYPE)
    {
        char* boolRegName = NULL;
        codeGenPrepareRegister(INT_REG, boolExpression->registerIndex, 1, 0, &boolRegName);
        fprintf(g_codeGenOutputFp, "beqz %s, _elseLabel_%d\n", boolRegName, labelNumber);
        freeRegister(INT_REG, boolExpression->registerIndex);
    }
    else if(boolExpression->dataType == FLOAT_TYPE)
    {
        fprintf(g_codeGenOutputFp, "l.s %s, _CONSTANT_%d\n", floatWorkRegisterName[0], constantZeroLabelNumber);
        char* boolRegName = NULL;
        codeGenPrepareRegister(FLOAT_REG, boolExpression->registerIndex, 1, 1, &boolRegName);
        fprintf(g_codeGenOutputFp, "c.eq.s %s, %s\n", boolRegName, floatWorkRegisterName[0]);
        fprintf(g_codeGenOutputFp, "bc1t _elseLabel_%d\n", labelNumber);
        freeRegister(FLOAT_REG, boolExpression->registerIndex);
    }

    AST_NODE* ifBodyNode = boolExpression->rightSibling;
    codeGenStmtNode(ifBodyNode);
    
    fprintf(g_codeGenOutputFp, "j _ifExitLabel_%d\n", labelNumber);
    fprintf(g_codeGenOutputFp, "_elseLabel_%d:\n", labelNumber);
    AST_NODE* elsePartNode = ifBodyNode->rightSibling;
    codeGenStmtNode(elsePartNode);
    fprintf(g_codeGenOutputFp, "_ifExitLabel_%d:\n", labelNumber);
}


void codeGenReturnStmt(AST_NODE* returnStmtNode)
{
    /* Find return type */
    AST_NODE* parentNode = returnStmtNode->parent;
    DATA_TYPE returnType = NONE_TYPE;
    while(parentNode)
    {
        if(parentNode->nodeType == DECLARATION_NODE)
        {
            if(parentNode->semantic_value.declSemanticValue.kind == FUNCTION_DECL)
            {
                returnType = parentNode->child->dataType;
            }
            break;
        }
        parentNode = parentNode->parent;
    }

    AST_NODE* returnVal = returnStmtNode->child;
    if(returnVal->nodeType != NUL_NODE)
    {
        codeGenExprRelatedNode(returnVal);

        char* returnValRegName = NULL;
        if (returnType == INT_TYPE)
        {
            /* Type conversion */
            if (returnVal->dataType == FLOAT_TYPE)
                returnVal->registerIndex = codeGenConvertFromFloatToInt(returnVal->registerIndex);

            codeGenPrepareRegister(INT_REG, returnVal->registerIndex, 1, 0, &returnValRegName);
            fprintf(g_codeGenOutputFp, "move $v0, %s\n", returnValRegName);
            freeRegister(INT_REG, returnVal->registerIndex);
        }
        else if(returnType == FLOAT_TYPE)
        {
            /* Type conversion */
            if (returnVal->dataType == INT_TYPE)
                returnVal->registerIndex = codeGenConvertFromIntToFloat(returnVal->registerIndex);

            codeGenPrepareRegister(FLOAT_REG, returnVal->registerIndex, 1, 0, &returnValRegName);
            fprintf(g_codeGenOutputFp, "mov.s $f0, %s\n", returnValRegName);
            freeRegister(FLOAT_REG, returnVal->registerIndex);
        }
    }
    fprintf(g_codeGenOutputFp, "j _end_%s\n", g_currentFunctionName); 
}


void codeGenStmtNode(AST_NODE* stmtNode)
{
    printSourceFile(g_codeGenOutputFp, stmtNode->linenumber);

    if(stmtNode->nodeType == NUL_NODE)
    {
        return;
    }
    else if(stmtNode->nodeType == BLOCK_NODE)
    {
        codeGenBlockNode(stmtNode);
    }
    else
    {
        switch(stmtNode->semantic_value.stmtSemanticValue.kind)
        {
        case WHILE_STMT:
            codeGenWhileStmt(stmtNode);
            break;
        case FOR_STMT:
            codeGenForStmt(stmtNode);
            break;
        case ASSIGN_STMT:
            codeGenAssignmentStmt(stmtNode);
            if(stmtNode->child->dataType == INT_TYPE)
            {
                freeRegister(INT_REG, stmtNode->child->registerIndex);
            }
            else if(stmtNode->child->dataType == FLOAT_TYPE)
            {
                freeRegister(FLOAT_REG, stmtNode->child->registerIndex);
            }
            break;
        case IF_STMT:
            codeGenIfStmt(stmtNode);
            break;
        case FUNCTION_CALL_STMT:
            codeGenFunctionCall(stmtNode);
            if(stmtNode->registerIndex != -1)
            {
                if(stmtNode->dataType == INT_TYPE)
                {
                    freeRegister(INT_REG, stmtNode->registerIndex);
                }
                else if(stmtNode->dataType == FLOAT_TYPE)
                {
                    freeRegister(FLOAT_REG, stmtNode->registerIndex);
                }
            }
            break;
        case RETURN_STMT:
            codeGenReturnStmt(stmtNode);
            break;
        default:
            printf("Unhandle case in void processStmtNode(AST_NODE* stmtNode)\n");
            break;
        }
    }
}


void codeGenGeneralNode(AST_NODE* node)
{
    AST_NODE *traverseChildren = node->child;
    AST_NODE *lastChild = NULL;
    switch(node->nodeType)
    {
    case VARIABLE_DECL_LIST_NODE:
		// TODO: Initialization [Written]
		codeGenLocalVariableInitialization(node);
        break;
    case STMT_LIST_NODE:
        while(traverseChildren)
        {
            codeGenStmtNode(traverseChildren);
            traverseChildren = traverseChildren->rightSibling;
        }
        break;
    case NONEMPTY_ASSIGN_EXPR_LIST_NODE:
        while(traverseChildren)
        {
            codeGenAssignOrExpr(traverseChildren);
            if(traverseChildren->rightSibling)
            {
                if(traverseChildren->dataType == INT_TYPE)
                {
                    freeRegister(INT_REG, traverseChildren->registerIndex);
                }
                else if(traverseChildren->dataType == FLOAT_TYPE)
                {
                    freeRegister(FLOAT_REG, traverseChildren->registerIndex);
                }
            }
            lastChild = traverseChildren;
            traverseChildren = traverseChildren->rightSibling;
        }
        if (lastChild) {
            node->registerIndex = lastChild->registerIndex;
        }
        break;
    case NONEMPTY_RELOP_EXPR_LIST_NODE:
        // Why free register, dafaq?
        while(traverseChildren)
        {
            codeGenExprRelatedNode(traverseChildren);
            if(traverseChildren->rightSibling)
            {
                if(traverseChildren->dataType == INT_TYPE)
                {
                    freeRegister(INT_REG, traverseChildren->registerIndex);
                }
                else if(traverseChildren->dataType == FLOAT_TYPE)
                {
                    freeRegister(FLOAT_REG, traverseChildren->registerIndex);
                }
            }
            lastChild = traverseChildren;
            traverseChildren = traverseChildren->rightSibling;
        }
        if (lastChild) {
            node->registerIndex = lastChild->registerIndex;
        }
        break;
    case NUL_NODE:
        break;
    default:
        printf("Unhandle case in void processGeneralNode(AST_NODE *node)\n");
        node->dataType = ERROR_TYPE;
        break;
    }
}
